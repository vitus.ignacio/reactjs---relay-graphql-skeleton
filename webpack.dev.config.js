const sharedConfiguration = require('./webpack.common');

module.exports = {
  mode: 'development',
  entry: sharedConfiguration.constants.ENTRY_POINT,
  module: {
    rules: sharedConfiguration.rules
  },
  output: {
    filename: sharedConfiguration.constants.OUTPUT.FILENAME,
    path: sharedConfiguration.constants.OUTPUT.PATH,
  },
  watch: true
}
