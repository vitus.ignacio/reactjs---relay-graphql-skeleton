export default (title, env, content, initialState) => {

  let
    includedJs = `
      <script src="main.app.js"></script>
    `, 
    page = '', 
    scripts = '',
    state = {};
  
  switch (env) {
    case 'staging':
    case 'prod':
      includedJs = `
        <script src="vendors~main.app.js"></script>
        <script src="main.app.js"></script>
      `
    default:
  }

  if (content) {
    scripts = `
      <script>
        window.__STATE__ = ${JSON.stringify(initialState ? initialState : state)};
      </script>
      ${includedJs}
      `
    }
  else {
    scripts = includedJs
  }

  page = `
    <!doctype html>
    <html lang="en" data-framework="relay">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>${title ? title.trim() : '...'}</title>
        <style>
          .wrapper {
            margin: 1.5em 0;
          }
        </style>
      </head>
      <body>
        <div id="root"></div>
        ${scripts}
      </body>
    </html>
  `
  return page;

}
