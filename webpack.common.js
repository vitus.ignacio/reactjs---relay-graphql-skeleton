const path = require('path');

const sharedConstants = {
  ENTRY_POINT: path.resolve(__dirname, 'js', 'app.js'),
  OUTPUT: {
    FILENAME: '[name].app.js',
    PATH: path.resolve(__dirname, 'public')
  }
}

const CSSModuleLoader = {
  loader: 'css-loader',
  options: {
    importLoaders: 2,
    modules: true,
    camelCase: false,
    localIdentName: '[name]__[local]___[hash:base64:5]'
  }
}

module.exports = {
  constants: sharedConstants,
  rules: [
    {
      test: /\.js$/,
      exclude: /\/node_modules\//,
      use: {
        loader: 'babel-loader'
      },
    },
    {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.scss$/,
      use: ['style-loader', CSSModuleLoader, 'sass-loader']
    }
  ],
}