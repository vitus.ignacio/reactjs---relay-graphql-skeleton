import express from 'express';
// Notes: This one is used for server-side JSON responses
// import graphQLHTTP from 'express-graphql';
// import {schema} from './data/schema';

// Application-specific imports
import template from './templates/default.layout';
import { runInNewContext } from 'vm';

const APP_ENVIRONMENT = process.argv[2] || 'dev'
const APP_PORT = APP_ENVIRONMENT === 'dev' ? 3000 : 80;

const app = express()

if (APP_ENVIRONMENT === 'prod') {
  app.get('*.js', (req, res, next) => {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/javascript');
    next();
  })
}

app.use(express.static('public'));

// Serve static resources
// app.use('/', express.static(path.resolve(__dirname, 'public')));

// Notes: This one is used for server-side JSON responses
// Setup GraphQL endpoint
// app.use(
//   '/graphql',
//   graphQLHTTP({
//     schema: schema,
//     pretty: true,
//   }),
// );

app.get('*', (req, res) => {
  const response = template('', APP_ENVIRONMENT);
  res.setHeader('Cache-Control', 'assets, max-age=604800');
  res.send(response);
})

app.listen(APP_PORT, () => {
  console.log(`App is now running on http://localhost:${APP_PORT}`);
});
