// TODO: Modify the code below for actual application
// TODO: Uncomment this line when this is required ```import React from 'react'```;
// TODO: Read differences amongst mount, render and shallow here https://gist.github.com/fokusferit/e4558d384e4e9cab95d04e5f35d4f913
// TODO: Read informations about chai expectation and assertions here https://www.chaijs.com/api/bdd/
import { mount, render, shallow } from 'enzyme';
import { expect } from 'chai';

describe('Demo test suite', () => {

  it('default test case', () => {
    expect(1).to.equal(1);
  })

  describe('Demo test suite - sub-test suites', () => {

    it('first test case', () => {
      expect(0).to.equal(0);
    })

  })

})
