import cookies from 'react-cookies';

import { APPLICATION_CONSTANTS } from './constants';

export const LanguageHelper = {
  persistLanguageFromCookie: (i18n) => {
    if (i18n) {
      let defaultLanguage = cookies.load('lang');
      if (defaultLanguage) {
        i18n.changeLanguage(APPLICATION_CONSTANTS.DEFAULT_LANGUAGE);
      }
    }
  },
  setLocaleApplicationTitle: (document, i18n) => {
    document.getElementsByTagName('title')[0].innerHTML = i18n.t('application_title');
  }
}

export const FormHelper = {
  // Validate form and show error messages if any. Ref: https://codepen.io/_arpy/pen/xYoyPW?editors=0010
  validateForm: (formEl) => {
    const formLength = formEl.length;

    if (formEl.checkValidity() === false) {
      for (let i = 0; i < formLength; i++) {
        const elem = formEl[i];
        const errorLabel = elem.parentNode.querySelector('.validation-message');
        if (errorLabel && elem.nodeName.toLowerCase() !== 'button') {
          if (!elem.validity.valid) {
            errorLabel.textContent = elem.validationMessage;
          } else {
            errorLabel.textContent = '';
          }
        }
      }

      return false;
    } else {
      for (let i = 0; i < formLength; i++) {
        const elem = formEl[i];
        const errorLabel = elem.parentNode.querySelector('.validation-message');
        if (errorLabel && elem.nodeName.toLowerCase() !== 'button') {
          errorLabel.textContent = '';
        }
      }

      return true;
    }
  }
}
