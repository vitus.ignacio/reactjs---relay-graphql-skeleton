// TODO: Modify the code below for actual application
import { TYPES } from './actions'

const initialStates = {
  formComponent: {
    text: 'demo text display placeholder'
  }
}

export const data = (state = initialStates, action) => {
  switch (action.type) {
    case TYPES.SET_TEXT:
      return Object.assign({}, state, {
        formComponent: {
          text: action.text 
        }
      })
    default:
      return state
  }
}
