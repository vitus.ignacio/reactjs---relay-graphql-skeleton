// TODO: Modify the code below for actual application

export const TYPES = {
  SET_TEXT: 'SET_TEXT'
}

export const actions = {
  setText: (text) => ({ type: TYPES.SET_TEXT, text })
}
