import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import WrapperComponent from './components/shared/layouts/WrapperComponent';
import ErrorComponent from './components/shared/ErrorComponent';
import FormComponent from './components/demo/FormComponent';
import FriendsComponent from './components/demo/FriendsComponent';

import i18n from './i18n/config';
import { LanguageHelper } from './utilities/helpers'; 

import store from './redux/demo/stores';

import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './styles/all.scss';

// Persist application based on cookie named `lang`
LanguageHelper.persistLanguageFromCookie(i18n);
// Set application title based on language
LanguageHelper.setLocaleApplicationTitle(document, i18n);

// TODO: Modify the code below for actual application
const routing = (
  <WrapperComponent>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path='/friends' component={FriendsComponent} />} />
          <Route path='/form' component={FormComponent} />} />
          <Route component={ErrorComponent} />
        </Switch>
      </Router>
    </Provider>
  </WrapperComponent>
)

ReactDOM.render(
  routing,
  document.getElementById('root'),
);
