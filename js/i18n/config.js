import i18next from 'i18next';

import { APPLICATION_CONSTANTS } from '../utilities/constants'

import en from './locales/en';
import ja from './locales/ja';
import others from './locales/others';

i18next.init({
    resources: Object.assign(en, ja, others),
    lng: APPLICATION_CONSTANTS.DEFAULT_LANGUAGE,
    fallbackLng: APPLICATION_CONSTANTS.DEFAULT_LANGUAGE,
    interpolation: {
      escapeValue: false
    }
  });

export default i18next;
