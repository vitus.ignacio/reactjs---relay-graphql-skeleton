import React from 'react';

import {QueryRenderer, graphql} from 'react-relay';
import {Environment, Network, RecordSource, Store} from 'relay-runtime';

import HeaderComponent from '../shared/HeaderComponent';

import styles from './FriendsComponent.scss';

export default class FriendsComponent extends React.Component {

  fetchQuery = () => {
    return fetch('/data.json', {
      method: 'GET',
    }).then(response => {
      return response.json();
    });
  }
  
  environment = new Environment({
    network: Network.create(this.fetchQuery),
    store: new Store(new RecordSource()),
  });
  
  render() {
    return(
      <QueryRenderer
        environment={this.environment}
        query={graphql`
          query FriendsComponentQuery {
            friends {
              id
              name
            }
          }
        `}
        variables={{}}
        render={({error, props}) => {
          if (props) {
            return (
              <div>
                <HeaderComponent />
                <div className={styles['grid-container']}>
                  <div className={styles['grid-item-row']}>
                    {props.friends.map( (friend) => {
                      return (
                        <div key={friend.id} className={styles['grid-item']}>
                          <div className={styles['grid-item__wrapper']}>
                            <div className={styles['grid-item__wrapper__container']}>
                              <div className={styles['grid-item__wrapper__container__image-top']}></div>
                              <div className={styles['grid-item__wrapper__container__content']}>
                                <span className={styles['grid-item__wrapper__container__content__name']}>{friend.name}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            );
          } else {
            return (
              <div>
                <HeaderComponent />
              </div>
            );
          }
        }}
      />
    )
  }

}