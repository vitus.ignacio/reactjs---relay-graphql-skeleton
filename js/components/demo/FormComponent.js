import React from 'react';
import { connect } from 'react-redux'
import i18n from '../../i18n/config';

import { 
  Container, 
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';

import { actions } from '../../redux/demo/actions';

// import styles from './FormComponent.scss';

const mapStateToProps = (state) => {
  return { formTextFieldValue: state.data.formComponent.text };
}

class FormComponent extends React.Component {

  render() {
    return (
      <Container className={`wrapper`}>
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <Label for='textField'>{i18n.t('demo_translation')}</Label>
                <Input
                  type='text'
                  name='textField'
                  id='textField'
                  placeholder='Put some text here'
                  onChange={(event) => {
                    this.props.dispatch(actions.setText(event.target.value));
                  }}
                />
              </FormGroup>
              <FormGroup>
                <span>{this.props.formTextFieldValue}</span>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }

}

export default connect(mapStateToProps)(FormComponent)
