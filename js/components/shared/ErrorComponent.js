import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import i18n from '../../i18n/config';

import styles from './ErrorComponent.scss';

export default class ErrorComponent extends React.Component {

  render() {
    return (
      <Container className={`wrapper`}>
        <Row>
          <Col>
            <div className={styles.error}>
              <div className={styles.error__wrapper}>
                <h1 className={styles.error__wrapper__code}>404</h1>
                <p className={styles.error__wrapper__message}>{i18n.t('errors.404_message')}</p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    )
  }

}
