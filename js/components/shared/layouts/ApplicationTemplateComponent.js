import React from 'react';

export default class ApplicationTemplateComponent extends React.Component {

  render() {
    return <div>
      {this.props.children}
    </div>
  }

}
