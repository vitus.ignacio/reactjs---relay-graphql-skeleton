import React from 'react';

import {
  Navbar,
  NavbarBrand
} from 'reactstrap';

export default class HeaderComponent extends React.Component {

  render() {
    return (
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">React Web Application</NavbarBrand>
      </Navbar>
    )
  }

}