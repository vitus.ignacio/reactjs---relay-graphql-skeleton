const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const sharedConfiguration = require('./webpack.common');

module.exports = {
  mode: 'production',
  entry: sharedConfiguration.constants.ENTRY_POINT,
  optimization: {
    minimizer: [
      new CompressionPlugin({
        test: /\.js(\?.*)?$/i
      }),
      new UglifyJsPlugin({
        test: /\.js(\?.*)?$/i,
        cache: true,
        parallel: true,
        sourceMap: true
      })
    ],
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: sharedConfiguration.rules
  },
  output: {
    filename: sharedConfiguration.constants.OUTPUT.FILENAME,
    path: sharedConfiguration.constants.OUTPUT.PATH,
  },
  watch: false
}
